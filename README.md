# Site Zero

Site zero é exagero, pois já parti de um bom templalte, do Bootstrap e de um bom menu para as seções internas e vários outros bons recursos. Apenas fui juntando as peças até chegar onde está.

O grande objetivo é o de criação de um site apenas com Conteúdo Estático e hospedado no GitHub, gratuitamente.
Com bons e simples recursos para gerenciamento do conteúdo
Lembrando que todo o conteúdo é adicionado manualmente, o que acarreta mais trabalho mas mais controle

Criar uma estrutura que que permita criar um bom site, além de contar com bons recursos e grande controle por conta da simpicidade dos elementos integrantes.
Lembrando que não temos bancos de dados e não haverá interatividade com o visitante. Apenas conteúdo estático

## O que contém?
Este site contém o menu de entrada e o menu interno, para adição de conteúdo.

Já acompanha a sub seção "Básicos" com pouca coisa.

Você pode alterar o que bem entender, mas especialmente a seção Básicos, que está aí apenas como modelo, para que aproveite o arquivo index.html e menu.html para sair criando seu conteúdo com um subdiretório para cada assunto.

Não obrigatório, mas recomendado que seja usado por quem entende de HTML e CSS.

https://github.com/ribafs/site-zero

Para mais detalhes veja os documentos na pasta docs.# Site from Zero

Projeto de "Sites do zero" com a intenção de criar sites ou blogs "do zero" e ensinar a abrigar sites no GitHub e desmonstrar a usabilidade do deste projeto, como site usável e de hospedagem gratuito.

Do zero é exagero, pois começo usando um template de terceiros, depois adiciono a biblioteca Bootstrap, depois a jquery e um template dropdown, multilevel e fixo à esquerda e topo. O zero é porque uso tudo isso "na mão".

Criação de um Site apenas com Conteúdo Estático
Com bons e simples recursos para gerenciamento do conteúdo
Alguns sites no podem usar somente estes recursos, mas alguns como o meu (https://github.com/ribafs/ribafs.github.io) podem.

Achei que seria interessante mudar o nome deste projeto para Static Site, pois esta é a ideia, criar uma estrutura que forneça as facilidades de gerenciamento de conteúdo, além de contar ainda com bons recursos e grande controle por conta da simpicidade dos elementos integrantes.

https://github.com/ribafs/site-zero

Para mais detalhes veja os arquivos do diretório docs.

O RibaFS Portal foi criado usando este projeto e está bem mais atualizado e conta já com alguns templates adaptados para este projeto.

Objetivo Principal

Isto é antes de tudo um roteiro com regras e dicas para a criação de sites estáticos.

https://github.com/ribafs/ribafs.github.io

Veja a pasta docs para mais informações.
E alguns templates na pasta templates.

Veja detalhes:

https://ribafs.github.io/staticsite/index.html

Além de um exemplo de uso

https://github.com/ribafs/ribafs.github.io

## Templates
Na pasta templates já existem 4 templates para substituição do atual.

### Templates deste Site
Este site é composto de 2 templates, um para a entrada, com alguns menus e bem acabado com um menu pequeno ao centro e acima.
E outro template mais leve destinado ao abrigo de conteúdo e impressão, com o menu muito flexível, latereal à esquerda.
